#ifndef CANDIDATO_HPP
#define CANDIDATO_HPP

#include <iostream>
#include <string>
#include "pessoa.hpp"

using namespace std;

class Candidato: public Pessoa{
    private:
    //string nome;
    string cargo;
    string uf;
    string partido;
    int numero;
    int ocorrencia;
    //Adendo adendo1;
    //Adendo adendo2;

    public:
    Candidato();
    // ~Candidato();
    //string get_nome();
    //void set_nome(string nome);
    string get_cargo();
    void set_cargo(string cargo);
    string get_uf();
    void set_uf(string uf);
    string get_partido();
    void set_partido(string partido);
    int get_numero();
    void set_numero(int numero);
    int get_ocorrencia();
    void set_ocorrencia(int ocorrencia);
    //Adendo get_adendo1();
    //void set_adendo1(string nome, string cargo, string uf);
    //Adendo get_adendo2();
    //void set_adendo2(string nome, string cargo, string uf);
};

#endif