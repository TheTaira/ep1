#ifndef PESSOA_HPP
#define PESSOA_HPP

#include <string>

using namespace std;

class Pessoa{
    protected:
    string nome;
    int cpf;

    public:
    Pessoa();
    string get_nome();
    void set_nome(string nome);
    int get_cpf();
    void set_cpf(int cpf);
};

#endif