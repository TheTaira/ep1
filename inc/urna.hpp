#ifndef URNA_HPP
#define URNA_HPP

#include <iostream>
#include <string>
#include <vector>
#include "candidato.hpp"

using namespace std;

class Urna {
    private:
    string nome;

    public:
    Urna();
    //~Urna();
    Candidato cria_candidatos(string file_name, int linha);
    string remove_char(string str);
    vector <Candidato> voto_senador(int numero, vector <Candidato> vetor);
    vector <Candidato> voto_governador(int numero, vector <Candidato> vetor);
    vector <Candidato> voto_presidente(int numero, vector <Candidato> vetor);
    Candidato voto_deputado_federal(int numero, vector <Candidato> vetor);
    Candidato voto_deputado_distrital(int numero, vector <Candidato> vetor);
    Candidato find_candidato_ganhador(vector <Candidato> candidato_ganhador);
    vector <Candidato> find_vetor_ganhador(vector <vector <Candidato> > vetor_ganhador);

    vector <Candidato> empate(vector <Candidato> candidato_ganhador);
    vector < vector <Candidato> > empate(vector < vector <Candidato> > vetor_ganhador);
    
};

#endif