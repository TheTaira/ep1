#ifndef VOTO_HPP
#define VOTO_HPP

#include <vector>
#include "eleitor.hpp"
#include "candidato.hpp"

using namespace std;
class Voto{
    private:
    Eleitor eleitor;
    Candidato deputado_federal, deputado_distrital;
    vector <Candidato> senador, governador, presidente;
    
    public:
    Voto();
    Eleitor get_eleitor();
    void set_eleitor(Eleitor eleitor);
    Candidato get_deputado_federal();
    void set_deputado_federal(Candidato deputado_federal);
    Candidato get_deputado_distrital();
    void set_deputado_distrital(Candidato deputado_distrital);
    vector <Candidato> get_senador();
    void set_senador(vector <Candidato> senador);
    vector <Candidato> get_governador();
    void set_governador(vector <Candidato> governador);
    vector <Candidato> get_presidente();
    void set_presidente(vector <Candidato> presidente);
};

#endif