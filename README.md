# Exercício de Programação 1

Acesse a descrição completa deste exercício na [wiki](https://gitlab.com/oofga/eps_2018_2/ep1/wikis/Descricão)

## Como usar o projeto

* Compile o projeto com o comando:

```sh
make
```

* Execute o projeto com o comando:

```sh
make run
```

## Funcionalidades do projeto

Este é um simulador de votação eletronica no Distrito Federal. É possível votar em deputado federal, deputado distrital, senador, governador e presidente.

ão ser iniciado, depois de os dados serem carregados o programa da a opção de digitar quantos eleitores votarão e então começa o loop de votação.

#### Loop de votação

O voto de cada eleitor começa com seu nome sendo pedido pelo programa. É dada então opção de voto para Deputado Federal, Deputado Distrital, Senador, Governador e então Presidente.

Para cada candidato a ser votado o programa pede uma entrada referente ao número do candidato em que se deseja votar. Uma entrada de um número não correspondente a nenhum candidato do cargo que esta sendo votado e interpretada como voto nulo, nesse momento e possível digitar a entrada "branco" para indicar voto em branco.

Depois de ter sido inserida a entrada o programa pergunta ao usuario se quer confirmar ou corrigir sua opção, imprimindo na tela os dados do candidato referido pela entrada ou "voto nulo" quando o voto tiver sido nulo ou "voto em branco" se o voto tiver sido em branco. Neste momento e possível digitar "confirma" ou "corrige" para indicar sua opção.

Depois de todos os eleitores terem votado em todos os cargos e emitido um relatorio contendo todos os votos de todos os eleitores, incluindo votos nulos e brancos,e então os candidatos ganhadores nesta votação, incluindo os candidatos que estiverem empatados caso isso aconteça.

## Bugs e problemas

Devido ao uso da função cin para entradas do usuario é possível encontrar erros ao se digitar números seguidos de letras em entradas como as dos números dos candidatos em que se deseja votar e na referente ao número de eleitores.

Na entrada referente ao nome do eleitor é preciso que nomes compostos sejam separados com "_" ou "-" pois a função cin identifica strings separadas com espaços ou entradas que começam com números e acaba com letras como duas entradas diferentes, senndo que a segunda fica na stream e é absorvida pela proxima entrada.

Caso todos os eleitores tenham votado em branco ou nulo o programa não consegue identificar que não houve ganhadores.


## Referências

Simulador de voto do TSE:

http://www.tse.jus.br/eleicoes/eleicoes-2018/simulador-de-votacão-na-urna-eletronica

Repositorio de dados eleitorais do TSE:

http://www.tse.jus.br/eleicoes/estatisticas/repositorio-de-dados-eleitorais-1/repositorio-de-dados-eleitorais