//#include <iostream>

#include <iostream>
#include <string>
#include <vector>
#include "candidato.hpp"

#include <string>
#include <fstream>
#include <sstream>
#include "urna.hpp"

using namespace std;

Urna::Urna(){
    nome ="jose";}

string Urna::remove_char(string str){
    string aux;
    int aspas=0;

    for(int j=0; j>=0; j++){

        if ('"' == str[j]){
            aspas++;
            if (aspas==2)
                return aux;
        
        }else{
            aux.push_back( str[j] );
        }
    }
    return aux;
}

Candidato Urna::cria_candidatos(string file_name, int linha){

    string string_aux;
    ifstream file;

    int int_aux;
    int i;
    int j;
    int file_open=0;

    int DS_CARGO =0, NM_URNA_CANDIDATO=0, NR_CANDIDATO=0, NM_PARTIDO=0, NM_UE=0;

    Candidato candidato1;


    try{
        file.open(file_name.c_str());
        file_open=1;
    }catch(...){
        cout <<"ndeu"<<endl;
    }
    


    

    while(getline(file,string_aux, ';')){ 

        if (string_aux == "\"DS_CARGO\"" && DS_CARGO==0){
            i=0;
            while(i<(57*linha)){
                getline(file, string_aux, ';');
                i++;
            }
            candidato1.set_cargo(remove_char(string_aux));
            DS_CARGO = 1;
            file.close();
            file.open(file_name.c_str());
        }



        if (string_aux == "\"NM_URNA_CANDIDATO\"" && NM_URNA_CANDIDATO==0){
            i=0;
            while(i<(57*linha)){
                getline(file, string_aux, ';');
                i++;
            }
            candidato1.set_nome(remove_char(string_aux));
            NM_URNA_CANDIDATO = 1;
            file.close();
            file.open(file_name.c_str());
        }



        if (string_aux == "\"NR_CANDIDATO\"" && NR_CANDIDATO==0){
            i=0;
            while(i<(57*linha)){
                getline(file, string_aux, ';');
                i++;
            }
            
            stringstream lol(remove_char(string_aux));

            lol >> int_aux;

            candidato1.set_numero(int_aux);
            NR_CANDIDATO = 1;
            file.close();
            file.open(file_name.c_str());
        }
        


        if (string_aux == "\"NM_PARTIDO\"" && NM_PARTIDO==0){
            i=0;
            while(i<(57*linha)){
                getline(file, string_aux, ';');
                i++;
            }

            candidato1.set_partido(remove_char(string_aux));
            NM_PARTIDO = 1;
            file.close();
            file.open(file_name.c_str());
        }



        if (string_aux == "\"NM_UE\"" && NM_UE==0){
            i=0;
            while(i<(57*linha)){
                getline(file, string_aux, ';');
                i++;
            }

            candidato1.set_uf(remove_char(string_aux));
            NM_UE = 1;
            file.close();
            file.open(file_name.c_str());
        }
    }
    candidato1.set_ocorrencia(0);
    //cout <<candidato1.get_ocorrencia();
    return candidato1;
}



vector <Candidato> Urna::voto_senador(int numero, vector <Candidato> vetor){

    int i=-1, j=0;
    Candidato candidato_senador, candidato_1_suplente, candidato_2_suplente;
    vector <Candidato> vetor_senador;

    do{
        do{
            i++;
            if (vetor[i].get_nome()=="nulo"){
                vetor[i].set_numero(numero);
                vetor[i].set_cargo("nulo");
            }
        }while(vetor[i].get_numero()!=numero);

        if (vetor[i].get_cargo()== "SENADOR"){
            candidato_senador = vetor[i];
        }else if(vetor[i].get_cargo()== "1 SUPLENTE"){
            candidato_1_suplente = vetor[i];
        }else if(vetor[i].get_cargo()== "2 SUPLENTE"){
            candidato_2_suplente = vetor[i];
        }else if(vetor[i].get_cargo()=="nulo"){
            vetor_senador.push_back(vetor[i]);
            vetor_senador.push_back(vetor[i]);
            vetor_senador.push_back(vetor[i]);
            return vetor_senador;
        }
        j++;
    
    }while(j!=3);

    vetor_senador.push_back(candidato_senador);
    vetor_senador.push_back(candidato_1_suplente);
    vetor_senador.push_back(candidato_2_suplente);

    return vetor_senador;

}

vector <Candidato> Urna::voto_governador(int numero, vector <Candidato> vetor){

    int i=-1, j=0;
    Candidato candidato_governador, candidato_vice_governador;
    vector <Candidato> vetor_governador;

    do{
        do{
            i++;
            if (vetor[i].get_nome()=="nulo"){
                vetor[i].set_numero(numero);
                vetor[i].set_cargo("nulo");
            }
        }while(vetor[i].get_numero()!=numero);

        if (vetor[i].get_cargo()== "GOVERNADOR"){
            candidato_governador = vetor[i];
            j++;
        }else if(vetor[i].get_cargo()== "VICE-GOVERNADOR"){
            candidato_vice_governador = vetor[i];
            j++;
        }else if(vetor[i].get_cargo()=="nulo"){
            vetor_governador.push_back(vetor[i]);
            vetor_governador.push_back(vetor[i]);
            return vetor_governador;
        }else{
            candidato_governador.set_cargo("not governador");
            candidato_vice_governador.set_cargo("not vice-governador");
        }

    
    }while(j!=2);

    vetor_governador.push_back(candidato_governador);
    vetor_governador.push_back(candidato_vice_governador);

    return vetor_governador;

}

vector <Candidato> Urna::voto_presidente(int numero, vector <Candidato> vetor){

    int i=-1, j=0;
    Candidato candidato_presidente, candidato_vice_presidente;
    vector <Candidato> vetor_presidente;

    do{
        do{
            i++;
            if (vetor[i].get_nome()=="nulo"){
                vetor[i].set_numero(numero);
                vetor[i].set_cargo("nulo");
            }
        }while(vetor[i].get_numero()!=numero);
  


        if (vetor[i].get_cargo()== "PRESIDENTE"){
            candidato_presidente = vetor[i];
            j++;
        }else if(vetor[i].get_cargo()== "VICE-PRESIDENTE"){
            candidato_vice_presidente = vetor[i];
            j++;
        }else if(vetor[i].get_cargo()=="nulo"){
            vetor_presidente.push_back(vetor[i]);
            vetor_presidente.push_back(vetor[i]);
            return vetor_presidente;
        }else{
            candidato_presidente.set_cargo("not presidente");
            candidato_vice_presidente.set_cargo("not vice-presidente");
        }
    
    }while(j!=2);

    vetor_presidente.push_back(candidato_presidente);
    vetor_presidente.push_back(candidato_vice_presidente);

    return vetor_presidente;

}

Candidato Urna::voto_deputado_federal(int numero, vector <Candidato> vetor){

    int i=-1, j=0;
    Candidato candidato_deputado_federal;
    do{
        do{
            i++;
            if (vetor[i].get_nome()=="nulo"){
                vetor[i].set_numero(numero);
                vetor[i].set_cargo("nulo");
            }
        }while(vetor[i].get_numero()!=numero);
        //cout << vetor[i].get_numero()<< endl;

        if (vetor[i].get_cargo()== "DEPUTADO FEDERAL"){
            candidato_deputado_federal = vetor[i];
            //cout<<"o cargo federal ta ceto"<<endl;
            j++;
        }else if(vetor[i].get_cargo()=="nulo"){
            return vetor[i];
        }
        else{
        //cout << "ta aqui no else n sei pq"<<endl;
        //cout <<vetor[i].get_numero()<< endl << vetor[i].get_cargo()<< endl;

        }
    }while(j!=1);

    return candidato_deputado_federal;

}

Candidato Urna::voto_deputado_distrital(int numero, vector <Candidato> vetor){

    int i=-1, j=0;
    Candidato candidato_deputado_distrital;
    do{
        do{
            i++;
            if (vetor[i].get_nome()=="nulo"){
                vetor[i].set_numero(numero);
                vetor[i].set_cargo("nulo");
            }
        }while(vetor[i].get_numero()!=numero);

        if (vetor[i].get_cargo()== "DEPUTADO DISTRITAL"){
            candidato_deputado_distrital = vetor[i];
        }else if(vetor[i].get_cargo()=="nulo"){
            return vetor[i];
        }
        j++;
    
    }while(j!=1);

    return candidato_deputado_distrital;

}

Candidato Urna::find_candidato_ganhador(vector <Candidato> candidato_ganhador){
    //cout<<"\n\n\n\n\n";

    int maior_ocorrencia=0, k_da_maior_ocorrencia=0, empate, oco;
    long unsigned int k=0, i=0, j;

    for(i=0; i!=candidato_ganhador.size(); i++){
        candidato_ganhador[i].set_ocorrencia(1);
    }

    for(i=0; i!=candidato_ganhador.size(); i++){
        j=1;
        while((i+j)!=candidato_ganhador.size()){
           if(candidato_ganhador[i].get_nome()==candidato_ganhador[i+j].get_nome()){
               //cout<< "entrou no if" << endl;
               oco= candidato_ganhador[i].get_ocorrencia();
               //cout<<"coco:"<<oco<<endl;
               candidato_ganhador[i].set_ocorrencia(oco+1);
               //cout <<"newcoco "<< candidato_ganhador[i].get_ocorrencia()<<endl;
           }
           j++;
        }
    }//cout<<"\n\n\n\n\n";
    for(k=0; k!=candidato_ganhador.size(); k++){
        if(candidato_ganhador[k].get_ocorrencia()>maior_ocorrencia && candidato_ganhador[k].get_nome()!="nulo" &&  candidato_ganhador[k].get_nome()!="branco"){
            maior_ocorrencia=candidato_ganhador[k].get_ocorrencia();
            k_da_maior_ocorrencia = k;
            empate=0;
        }else if(candidato_ganhador[k].get_ocorrencia()==maior_ocorrencia && candidato_ganhador[k].get_nome()!="nulo" 
        && candidato_ganhador[k].get_nome()!="branco"){
            empate=1;
        }
    }
    if (empate==1){
        throw 1;
    }//cout<<"\n\n\n\n\n";
    return candidato_ganhador[k_da_maior_ocorrencia];
}

vector <Candidato> Urna::find_vetor_ganhador(vector <vector <Candidato> > vetor_ganhador){
    int maior_ocorrencia=-1, k_da_maior_ocorrencia=0, empate, oco;
    long unsigned int k, i, j;
   /* cout << "chegou na funcaqiuefweifsao "<<endl;
    cout <<"esta aqui";
    cout << "lolzao" ;
    cout << "lols " << vetor_ganhador.size() << endl;
    cout << vetor_ganhador[0][0].get_nome()<<endl;*/


    for(i=0; i!=vetor_ganhador.size(); i++){
        vetor_ganhador[i][0].set_ocorrencia(1);
    }

    for(i=0; i!=vetor_ganhador.size(); i++){
        j=1;
        while((i+j)!=vetor_ganhador.size()){
           if(vetor_ganhador[i][0].get_nome()==vetor_ganhador[i+j][0].get_nome()){
               //cout<< "entrou no if" << endl;
               oco= vetor_ganhador[i][0].get_ocorrencia();
               //cout<<"coco:"<<oco<<endl;
               vetor_ganhador[i][0].set_ocorrencia(oco+1);
               //cout <<"newcoco "<< vetor_ganhador[i][0].get_ocorrencia()<<endl;
           }
           j++;
        }
    }



    for(k=0; k!=vetor_ganhador.size(); k++ ){
       // cout << vetor_ganhador[k][0].get_ocorrencia() << endl;
        if(vetor_ganhador[k][0].get_ocorrencia()>maior_ocorrencia && vetor_ganhador[k][0].get_nome() != "nulo" && vetor_ganhador[k][0].get_nome() != "branco"){
            maior_ocorrencia=vetor_ganhador[k][0].get_ocorrencia();
            //cout << "oco " << vetor_ganhador[k][0].get_ocorrencia()<<endl;;
            k_da_maior_ocorrencia = k;
            empate=0;
        }else if(vetor_ganhador[k][0].get_ocorrencia()==maior_ocorrencia && vetor_ganhador[k][0].get_nome() != "nulo"
         && vetor_ganhador[k][0].get_nome() != "branco"){
            empate=1;
        }else{
            //cout<<"wat"<<endl;
        }
    }
    if (empate==1){
        cout << "vaitrouar1" << endl;
        throw 1;
    }
    return vetor_ganhador[k_da_maior_ocorrencia];
}
vector <Candidato> Urna::empate(vector <Candidato> candidato_ganhador){
    int maior_ocorrencia=0;
    long unsigned int k, i, j;
    vector <Candidato> empatados;
    //cout << "comeco";




    
    for(k=0; k!=candidato_ganhador.size(); k++ ){
        if(candidato_ganhador[k].get_ocorrencia()>maior_ocorrencia && candidato_ganhador[k].get_nome()!="nulo" &&  candidato_ganhador[k].get_nome()!="branco"){
            maior_ocorrencia=candidato_ganhador[k].get_ocorrencia();

        }
    }
    for(k=0; k!=candidato_ganhador.size(); k++ ){
        if(candidato_ganhador[k].get_ocorrencia()==maior_ocorrencia && candidato_ganhador[k].get_nome()!="nulo" &&  candidato_ganhador[k].get_nome()!="branco"){
            empatados.push_back(candidato_ganhador[k]);
        }
    }
return empatados;
}


vector < vector <Candidato> > Urna::empate(vector < vector <Candidato> > vetor_ganhador){
    int maior_ocorrencia=0;
    long unsigned int k;
    vector < vector <Candidato>> empatados;
    for(k=0; k!=vetor_ganhador.size(); k++ ){
        if(vetor_ganhador[k][0].get_ocorrencia()>maior_ocorrencia && vetor_ganhador[k][0].get_nome() != "nulo" && vetor_ganhador[k][0].get_nome() != "branco"){
            maior_ocorrencia=vetor_ganhador[k][0].get_ocorrencia();

        }
    }
    for(k=0; k!=vetor_ganhador.size(); k++ ){
        if(vetor_ganhador[k][0].get_ocorrencia()==maior_ocorrencia && vetor_ganhador[k][0].get_nome() != "nulo" && vetor_ganhador[k][0].get_nome() != "branco"){
            empatados.push_back(vetor_ganhador[k]);
        }
    }
    return empatados;
}

