#include <iostream>

#include <vector>

#include <string> 
#include "fstream"

#include "urna.hpp"
#include "candidato.hpp"
#include "eleitor.hpp"
#include "voto.hpp"



using namespace std;

int main() {
    int teste, ok, linha, numero_senador, numero_governador, numero_deputado_federal, numero_deputado_distrital, numero_presidente;
    //oco_past,
    //oco_new;
    unsigned long int i, quantidade_de_eleitores, eleitor_atual;

    string file_name1="data/consulta_cand_2018_DF.csv", file_name2 = "data/consulta_cand_2018_BR.csv", nome_eleitor_atual, botao, input_do_usuario;


    Candidato candidato_aux, candidato_deputado_distrital, candidato_deputado_federal, candidato_nulo, deputado_distrital_aux, deputado_federal_aux;
    Eleitor eleitor_aux;
    Voto voto_aux;
    
    vector <Candidato> vetor_de_candidatos, vetor_senador, vetor_governador, vetor_presidente, vetor_senador_aux, vetor_governador_aux, vetor_presidente_aux;
    vector <Eleitor> vetor_de_eleitores;
    vector <Voto> vetor_de_votos;

    vector <Candidato> deputado_federal_ganhador, deputado_distrital_ganhador ;
    vector <vector <Candidato> >  senador_ganhador, governador_ganhador, presidente_ganhador;

    Urna the_urna;
    Candidato candidato_branco;

    candidato_branco.set_nome("branco");
    candidato_branco.set_cargo("branco");
    candidato_branco.set_ocorrencia(0);


    cout << "Carregando dados..." << endl;



    for (linha=1; linha<1246; linha++){

        candidato_aux = the_urna.cria_candidatos(file_name1, linha);
        vetor_de_candidatos.push_back(candidato_aux);
    }

   for (linha=1; linha<27; linha++){

        candidato_aux = the_urna.cria_candidatos(file_name2, linha);
        vetor_de_candidatos.push_back(candidato_aux);
    }
    vetor_de_candidatos.push_back(candidato_nulo);

    cout <<endl<<endl<< "Insira a quantidade de eleitores" << endl; 
    cin >> quantidade_de_eleitores;
    //quantidade_de_eleitores = 2;

    for(eleitor_atual=0; eleitor_atual<quantidade_de_eleitores; eleitor_atual++){

        cout<<endl<<"Insira o nome do eleitor  (Separe nomes compostos com \"_\")"<<endl;
        cin >> nome_eleitor_atual;
        eleitor_aux.set_nome(nome_eleitor_atual);
        vetor_de_eleitores.push_back(eleitor_aux);
        voto_aux.set_eleitor(eleitor_aux);
        
        cout<<endl<<endl<<"DEPUTADO FEDERAL"<<endl<<endl<<endl;
        //inicio do voto em deputado federal
        do{
            do{
                cout << endl << "Digite o numero ou \"branco\" para voto em branco" << endl<<endl<<endl;
                cin >> input_do_usuario;
                ok=1;
                try{
                    teste=stoi(input_do_usuario);
                }catch(...){
                    if(input_do_usuario!="branco")
                        ok=0;
                }
            }while(ok==0);
            if(input_do_usuario!="branco"){
                numero_deputado_federal = stoi(input_do_usuario);
                candidato_deputado_federal = the_urna.voto_deputado_federal(numero_deputado_federal, vetor_de_candidatos);
                if(candidato_deputado_federal.get_nome()=="nulo"){
                    cout << "voto nulo"<< endl<<endl<<endl;
                }else{
                cout  << candidato_deputado_federal.get_cargo()<< ": " <<candidato_deputado_federal.get_nome() << endl;
                cout << "UF: " << candidato_deputado_federal.get_uf() << endl;
                cout << "Partido: "<<candidato_deputado_federal.get_partido()<<endl<<endl<<endl;
                }

            }else{
                candidato_deputado_federal.set_nome("branco");
                cout << "voto em branco" << endl<<endl<<endl;
            }
             do{
                cout << "Digite \"confirma\" para confirmar ou \"corrige\" para corrigir"<<endl;
                cin >> botao;
                //botao="confirma";
            
            }while(botao!="confirma" && botao!="corrige");
        }while(botao!="confirma");
        deputado_federal_ganhador.push_back(candidato_deputado_federal);
        //cout<< "esse e o nome do cara:" <<deputado_federal_ganhador[eleitor_atual].get_nome();
        voto_aux.set_deputado_federal(candidato_deputado_federal);
        //fim do voto em deputado federal

        cout<<endl<<endl<<"DEPUTADO DISTRITAL"<<endl<<endl<<endl;
        //inicio do voto em deputado distrital
        do{
            
            do{
                cout << endl << "Digite o numero ou \"branco\"para voto em branco" << endl<<endl<<endl;
                cin >> input_do_usuario;
                ok=1;
                try{
                    teste=stoi(input_do_usuario);
                }catch(...){
                    if(input_do_usuario!="branco")
                        ok=0;
                }
            }while(ok==0);
            if(input_do_usuario!="branco"){

                numero_deputado_distrital = stoi(input_do_usuario);
                
                candidato_deputado_distrital = the_urna.voto_deputado_distrital(numero_deputado_distrital, vetor_de_candidatos);
                if(candidato_deputado_distrital.get_nome()=="nulo"){
                    cout << "voto nulo"<< endl<<endl<<endl;
                }else{
                    cout << candidato_deputado_distrital.get_cargo()<< ": " <<candidato_deputado_distrital.get_nome() << endl;
                    cout << "UF: " << candidato_deputado_distrital.get_uf() << endl;
                    cout << "Partido: "<<candidato_deputado_distrital.get_partido()<<endl<<endl<<endl;
                }
            }else{
                candidato_deputado_distrital.set_nome("branco");
                cout << "voto em branco" << endl<<endl<<endl;
            }
             do{
                cout << "Digite \"confirma\" para confirmar ou \"corrige\" para corrigir"<<endl;
                cin >> botao;
                //botao="confirma";
            }while(botao!="confirma" && botao!="corrige");
        }while(botao!="confirma");

        candidato_deputado_distrital.set_ocorrencia((candidato_deputado_distrital.get_ocorrencia()+1));

        deputado_distrital_ganhador.push_back(candidato_deputado_distrital);
        voto_aux.set_deputado_distrital(candidato_deputado_distrital);
        //fim do voto em deputado distrital

        cout<<endl<<endl<<"SENADOR"<<endl<<endl<<endl;
        //inicio do voto em senador

        do{
             do{
                cout << endl << "Digite o numero ou \"branco\"para voto em branco" << endl<<endl<<endl;
                cin >> input_do_usuario;
                ok=1;
                try{
                    teste=stoi(input_do_usuario);
                }catch(...){
                    if(input_do_usuario!="branco")
                        ok=0;
                }
            }while(ok==0);
            if(input_do_usuario!="branco"){
                //cout<<"loinoul"<<input_do_usuario;

                numero_senador = stoi(input_do_usuario);

                vetor_senador = the_urna.voto_senador(numero_senador, vetor_de_candidatos);
                if(vetor_senador[0].get_nome()=="nulo"){
                    cout<<"voto nulo"<<endl<<endl<<endl;
                }else{
                    for(i=0; i<=2; i++){
                        cout << vetor_senador[i].get_cargo()<< ": " <<vetor_senador[i].get_nome() << endl;
                    }
                        cout << "UF: " <<vetor_senador[0].get_uf()<< endl;
                        cout << "Partido: "<<vetor_senador[0].get_partido()<<endl<<endl<<endl; 
                }
            }else{
                vetor_senador.push_back(candidato_branco);
                vetor_senador.push_back(candidato_branco);
                vetor_senador.push_back(candidato_branco);
                cout << "voto em branco"<< endl<<endl<<endl;
            }
           // botao="confirma";
             do{
                cout << "Digite \"confirma\" para confirmar ou \"corrige\" para corrigir"<<endl;
                cin >> botao;
                //botao="confirma";
            }while(botao!="confirma" && botao!="corrige");
        }while(botao!="confirma");
        cout <<"lol3";
        /*
        for(int lol=0; lol!=3; lol++){
            vetor_senador[i].set_ocorrencia(vetor_senador[i].get_ocorrencia()+1);
        }
        */
        senador_ganhador.push_back(vetor_senador);
        voto_aux.set_senador(vetor_senador);
        //fim do voto em senador

        cout<<endl<<endl<<"GOVERNADOR"<<endl<<endl<<endl;
        //inicio do voto em governador
        do{
             do{
                cout << endl << "Digite o numero ou \"branco\"para voto em branco" << endl<<endl<<endl;
                cin >> input_do_usuario;
                ok=1;
                try{
                    teste=stoi(input_do_usuario);
                }catch(...){
                    if(input_do_usuario!="branco")
                        ok=0;
                }
            }while(ok==0);
            if(input_do_usuario!="branco"){

                numero_governador = stoi(input_do_usuario);

                vetor_governador = the_urna.voto_governador(numero_governador, vetor_de_candidatos);
                if(vetor_governador[0].get_nome()=="nulo"){
                    cout << "voto nulo" << endl;
                }else{
                    for(i=0; i<=1; i++){
                        cout  << vetor_governador[i].get_cargo()<< ": " <<vetor_governador[i].get_nome() << endl;
                    }
                    cout << "UF: " <<vetor_governador[0].get_uf()<< endl;
                    cout << "Partido: "<<vetor_governador[0].get_partido()<<endl<<endl<<endl;
                
                }
            }else{
                vetor_governador.push_back(candidato_branco);
                vetor_governador.push_back(candidato_branco);
                vetor_governador.push_back(candidato_branco);
                cout << "voto em branco"<< endl<<endl<<endl;
            }
         do{
                cout << "Digite \"confirma\" para confirmar ou \"corrige\" para corrigir"<<endl;
                cin >> botao;
                //botao="confirma";
            }while(botao!="confirma" && botao!="corrige");
        }while(botao!="confirma");
        /*
        for(int lol=0; lol!=2; lol++){
            vetor_governador[i].set_ocorrencia(vetor_governador[i].get_ocorrencia()+1);
        }
        */
        governador_ganhador.push_back(vetor_governador);
        voto_aux.set_governador(vetor_governador);
        //fim do voto em governador

        cout<<endl<<endl<<"PRESIDENTE"<<endl<<endl<<endl;
        //inicio do voto em presidente
        do{
             do{
                cout << endl << "Digite o numero ou \"branco\"para voto em branco" << endl<<endl<<endl;
                cin >> input_do_usuario;
                ok=1;
                try{
                    teste=stoi(input_do_usuario);
                }catch(...){
                    if(input_do_usuario!="branco")
                        ok=0;
                }
            }while(ok==0);
            if(input_do_usuario!="branco"){
                numero_presidente = stoi(input_do_usuario);

                vetor_presidente = the_urna.voto_presidente(numero_presidente, vetor_de_candidatos);
                if(vetor_presidente[0].get_nome()=="nulo"){
                    cout<<"voto nulo"<< endl<<endl<<endl;
                }else{
                    for(i=0; i<=1; i++){
                        cout  << vetor_presidente[i].get_cargo()<< ": " <<vetor_presidente[i].get_nome() << endl;
                    }
                    cout << "UF: " <<vetor_presidente[0].get_uf()<< endl;
                    cout << "Partido: "<<vetor_presidente[0].get_partido()<<endl<<endl<<endl;
                }
            }else{
                vetor_presidente.push_back(candidato_branco);
                vetor_presidente.push_back(candidato_branco);
                cout << "voto em branco"<< endl<<endl<<endl;
            }
            do{
                cout << "Digite \"confirma\" para confirmar ou \"corrige\" para corrigir"<<endl;
                cin >> botao;
                //botao="confirma";
            }while(botao!="confirma" && botao!="corrige");
        }while(botao!="confirma");
        /*
        for(int lol=0; lol!=2; lol++){
            vetor_presidente[i].set_ocorrencia(vetor_presidente[i].get_ocorrencia()+1);
        }*/
        presidente_ganhador.push_back(vetor_presidente);
        voto_aux.set_presidente(vetor_presidente);
        //fim do voto em presidente
        vetor_de_votos.push_back(voto_aux);
    }

    for(i=0; i!=quantidade_de_eleitores; i++){
        eleitor_aux = vetor_de_votos[i].get_eleitor();
        deputado_federal_aux = vetor_de_votos[i].get_deputado_federal();
        deputado_distrital_aux = vetor_de_votos[i].get_deputado_distrital();
        vetor_senador_aux = vetor_de_votos[i].get_senador();
        vetor_governador_aux = vetor_de_votos[i].get_governador();
        vetor_presidente_aux = vetor_de_votos[i].get_presidente();
        cout << endl << "Eleitor: " << eleitor_aux.get_nome() << endl;
        cout << "Deputado Federal:\t" << deputado_federal_aux.get_nome() << endl;
        cout << "Deputado Distrital:\t" << deputado_distrital_aux.get_nome() << endl;
        cout << "Senador:\t\t" << vetor_senador_aux[0].get_nome() << endl;
        cout << "1o Suplente:\t\t" << vetor_senador_aux[1].get_nome() << endl;
        cout << "2o Suplente:\t\t" << vetor_senador_aux[2].get_nome() << endl;
        cout << "Governador:\t\t" << vetor_governador_aux[0].get_nome() << endl;
        cout << "Vice-Governador:\t" << vetor_governador_aux[1].get_nome() << endl;
        cout << "Presidente:\t\t" << vetor_presidente_aux[0].get_nome() << endl;
        cout << "vice-Presidente:\t" << vetor_presidente_aux[1].get_nome() << endl;
        cout << endl<<endl;
    }

    Candidato federal_escolhido, distrital_escolhido;
    vector <Candidato> federais_empatados, distritais_empatados, senador_escolhido, governador_escolhido, presidente_escolhido;
    vector < vector <Candidato> > senadores_empatados, governadores_empatados, presidentes_empatados;

    cout << "candidatos ganhadores" << endl;

    try{
        federal_escolhido=the_urna.find_candidato_ganhador(deputado_federal_ganhador);

        cout << "Deputado Federal:\t" << federal_escolhido.get_nome()<<endl;

    }catch(...){
        cout <<"Empate de Deputados Federais"<<endl <<"Deputados Federais empatados:"<<endl<<endl;
        federais_empatados=the_urna.empate(deputado_federal_ganhador);   
        for(i=0;i!=federais_empatados.size();i++){
            cout<<federais_empatados[i].get_nome()<<endl;
        }
    }
    cout<<endl;
    try{
        distrital_escolhido=the_urna.find_candidato_ganhador(deputado_distrital_ganhador);
        cout << endl << "Deputado Distrital:\t" << distrital_escolhido.get_nome()<<endl;
    }catch(...){
        distritais_empatados=the_urna.empate(deputado_distrital_ganhador);
        cout <<"Empate de Deputados Distritais"<<endl<<"Deputados Distritais empatados:"<<endl<<endl;
        for(i=0;i!=distritais_empatados.size();i++){
            cout<<distritais_empatados[i].get_nome()<<endl;
        }
    }
    cout<<endl;
    try{
        
        senador_escolhido=the_urna.find_vetor_ganhador(senador_ganhador);
        cout << endl << "Senador:\t" << senador_escolhido[0].get_nome()<<endl;
        cout << "1o suplente:\t" << senador_escolhido[1].get_nome()<<endl;
        cout << "2o suplente:\t" << senador_escolhido[2].get_nome()<<endl;
    }catch(...){
        cout << "Empate de Senadores"<<endl<<"Senadores empatados:"<<endl<<endl;
        senadores_empatados=the_urna.empate(senador_ganhador);
        for(i=0; i!=senadores_empatados.size(); i++){
            cout<<"Senador:\t"<<senadores_empatados[i][0].get_nome()<<endl;
            cout<<"1o suplente:\t"<<senadores_empatados[i][1].get_nome()<<endl;
            cout<<"1o suplente:\t"<<senadores_empatados[i][2].get_nome()<<endl<<endl;

        }
    }
    cout << endl;
    try{
        governador_escolhido=the_urna.find_vetor_ganhador(governador_ganhador);

        cout << endl << "Governador:\t\t" << governador_escolhido[0].get_nome()<<endl;
        cout << "Vice-Governador:\t" << governador_escolhido[1].get_nome()<<endl;
    }catch(...){
        cout << "Empate de Governadores"<<endl<<"Governadores Empatados"<<endl<<endl;
        governadores_empatados=the_urna.empate(governador_ganhador);
        for(i=0;i!=governadores_empatados.size();i++){
            cout<<"Governador:\t\t"<<governadores_empatados[i][0].get_nome()<<endl;
            cout<<"Vice-Governador:\t"<<governadores_empatados[i][1].get_nome()<<endl<<endl;
        }
    }
    cout <<endl;
    try{
        presidente_escolhido=the_urna.find_vetor_ganhador(presidente_ganhador);
        cout << endl << "Presidente:\t\t" << presidente_escolhido[0].get_nome()<<endl;
        cout << "Vice-Presidente:\t" << presidente_escolhido[1].get_nome()<<endl<<endl;

    }catch(...){
        cout <<"Empate de Presidentes"<<endl;
        presidentes_empatados=the_urna.empate(presidente_ganhador);
        for(i=0; i!=presidentes_empatados.size(); i++){
            cout << "Presidente:\t\t"<<presidentes_empatados[i][1].get_nome()<<endl;
            cout << "Vice-Presidente:\t"<<presidentes_empatados[i][1].get_nome()<<endl<<endl;
        }
        
    }

    //cout << "cabo";
    

    
    return 0;
}
