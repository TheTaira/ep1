#include "voto.hpp"

#include <vector>
#include "eleitor.hpp"
#include "candidato.hpp"

Voto::Voto(){
    Eleitor eleitor;
    Candidato deputado_federal;
    Candidato deputado_distrital;
    vector <Candidato> senador, governador, presidente;
}

Eleitor Voto::get_eleitor(){
    return eleitor;
}

void Voto::set_eleitor(Eleitor eleitor){
    this->eleitor=eleitor;
}

Candidato Voto::get_deputado_federal(){
    return deputado_federal;
}
void Voto::set_deputado_federal(Candidato deputado_federal){
    this->deputado_federal=deputado_federal;
}
Candidato Voto::get_deputado_distrital(){
    return deputado_distrital;
}
void Voto::set_deputado_distrital(Candidato deputado_distrital){
    this->deputado_distrital=deputado_distrital;
}
vector <Candidato> Voto::get_senador(){
    return senador;
}
void Voto::set_senador(vector <Candidato> senador){
    this->senador = senador;
}
vector <Candidato> Voto::get_governador(){
    return governador;
}
void Voto::set_governador(vector <Candidato> governador){
    this->governador = governador;
}
vector <Candidato> Voto::get_presidente(){
    return presidente;
}
void Voto::set_presidente(vector <Candidato> presidente){
    this->presidente = presidente;
}